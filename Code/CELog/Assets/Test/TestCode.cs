﻿using UnityEngine;

public class TestCode : MonoBehaviour
{
    public void PrintTestLog()
    {
        Debug.Log("This is normal debug.log");
        Debug.LogWarning("This is normal debug.warning");
        Debug.LogError("This is normal debug.error");
        Debug.Log("=======");
        CELog.Log("this is CELog only ,not show in console");
    }
}