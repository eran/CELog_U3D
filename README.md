#CELog_U3D

![WX20180203-113602](http://p09vx6soj.bkt.clouddn.com/WX20180203-113602.png)


### 使用方式
![WX20180203-113749](http://p09vx6soj.bkt.clouddn.com/WX20180203-113749.png)

将CELog拖入Canvas下面

代码中正常使用`Debug.Log`,`Debug.LogWarning`,`Debug.LogError`即可

```
Debug.Log("xxx");
Debug.LogWarning("xxx");
Debug.LogError("xxx");
```

Thanks

